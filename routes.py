# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import os

from collections import defaultdict
from functools import wraps

from werkzeug import Response
from werkzeug.datastructures import Headers
from werkzeug.middleware.shared_data import SharedDataMiddleware

from trytond.pool import Pool
from trytond.protocols.wrappers import (
    with_pool, with_transaction, user_application)
from trytond.transaction import Transaction
from trytond.wsgi import app
from tempfile import NamedTemporaryFile
from PIL import Image
import getpass

__all__ = ['shipment_in_application', 'get_employee']


def shipment_in_application(func):
    @wraps(func)
    @user_application('shipment_in')
    def wrapper(request, *args, **kwargs):
        ctx = {
            'company': None,
            'employee': None,
            }
        employee = get_employee(request)
        if employee:
            ctx['company'] = employee.company.id
            ctx['employee'] = employee.id
        else:
            User = Pool().get('res.user')
            user = User(Transaction().user)
            company = user.company or user.main_company
            if company:
                ctx['company'] = company.id
        with Transaction().set_context(ctx):
            return func(request, *args, **kwargs)
    return wrapper


def get_employee(request):
    pool = Pool()
    Employee = pool.get('company.employee')
    try:
        employee = int(request.headers['X-Employee'])
    except (KeyError, ValueError):
        return None
    return Employee(employee)


@app.route(
    '/<database_name>/flask_shipment_in_app/send_carrier_email',
    methods=['POST'])
@with_pool
@with_transaction()
@shipment_in_application
def send_carrier_email(request, pool):
    Shipment = pool.get('stock.shipment.in')
    _id = request.parsed_data.get('id')
    Shipment.send_carrier_email([_id])


@app.route(
    '/<database_name>/flask_shipment_in_app/upload_file', methods=['POST'])
@with_pool
@with_transaction()
@shipment_in_application
def upload_file(request, pool):
    Attachment = pool.get('ir.attachment')

    file = request.files['file']
    shipment = 'stock.shipment.in,%s' % request.form['id']

    extension = file.filename.split('.')[-1]
    with NamedTemporaryFile(mode='w+b', suffix=extension) as temp:
        image_temp = Image.open(file)
        format = image_temp.format

        base_width = 960
        x, y = image_temp.size
        width_percent = (base_width / float(x))
        hsize = int((float(y) * float(width_percent)))
        image_temp = image_temp.resize((base_width, hsize), Image.ANTIALIAS)
        image_temp.save(temp, format)
        with open(temp.name, 'rb') as f:
            _bytes = f.read()
        
    attachment = Attachment(name=file.filename, type='data',
        data=_bytes, resource=shipment)
    attachment.save()
    return {'id': attachment.id,
            'name': attachment.name,
            'data': attachment.data}


@app.route(
    '/<database_name>/flask_shipment_in_app/remove_file', methods=['POST'])
@with_pool
@with_transaction()
@shipment_in_application
def delete_file(request, pool):
    Attachment = pool.get('ir.attachment')

    attachment = Attachment(int(request.parsed_data.get('id')))
    Attachment.delete([attachment])


@app.route('/flask_shipment_in_app/', methods=['GET'])
def shipment_in_app(request):
    g = open(os.path.join(os.path.dirname(__file__), 'app', 'index.html'))
    return Response(
        g.read(),
        mimetype='text/html',
        direct_passthrough=True)


@app.route('/<database_name>/flask_shipment_in_app/employees', methods=['GET'])
@with_pool
@with_transaction()
@shipment_in_application
def employees(request, pool):
    User = pool.get('res.user')
    user = User(Transaction().user)
    return [{'id': e.id, 'name': e.rec_name} for e in user.employees]


@app.route(
    '/<database_name>/flask_shipment_in_app/warehouses', methods=['GET'])
@with_pool
@with_transaction()
@shipment_in_application
def warehouses(request, pool):
    Location = pool.get('stock.location')
    warehouses = Location.search([('type', '=', 'warehouse')])
    return [{'id': w.id, 'name': w.rec_name} for w in warehouses]


@app.route('/<database_name>/flask_shipment_in_app/parties', methods=['GET'])
@with_pool
@with_transaction()
@shipment_in_application
def parties(request, pool):
    Party = pool.get('party.party')
    domain = []
    query = request.args.get('query')
    if query:
        domain.append(('rec_name', 'ilike', '%%%s%%' % query))
    return [{
            'id': p.id,
            'name': p.rec_name,
            } for p in Party.search(domain)]


@app.route('/<database_name>/flask_shipment_in_app/agencies', methods=['GET'])
@with_pool
@with_transaction()
@shipment_in_application
def agencies(request, pool):
    Party = pool.get('party.party')
    domain = [('carrier_type', '=', 'agency')]
    query = request.args.get('query')
    if query:
        domain.append(('rec_name', 'ilike', '%%%s%%' % query))
    return [{
            'id': p.id,
            'name': p.rec_name,
            } for p in Party.search(domain)]


@app.route('/<database_name>/flask_shipment_in_app/products', methods=['GET'])
@with_pool
@with_transaction()
@shipment_in_application
def products(request, pool):
    Product = pool.get('product.product')
    domain = []
    query = request.args.get('query')
    if query:
        domain.append(('rec_name', 'ilike', '%%%s%%' % query))
    return [{
            'id': p.id,
            'name': p.rec_name
            } for p in Product.search(domain)]


@app.route('/<database_name>/flask_shipment_in_app/shipments', methods=['GET'])
@with_pool
@with_transaction()
@shipment_in_application
def shipments(request, pool):
    Shipment = pool.get('stock.shipment.in')

    limit = int(request.args.get('query[limit]', 20))
    done = request.args.get('query[done]', False)
    done = True if done == 'true' else False

    user_id = Transaction().user
    domain = [
        ('effective_date', '!=', None),
        ('create_uid', '=', user_id)]
    order = [('effective_date', 'DESC')]

    if done:
        domain.append(('state', '=', 'done'))
    else:
        domain.append(('state', '=', 'draft'))

    shipments = Shipment.search(domain, limit=limit, order=order)
    return [shipment.flask_shipment_in_app_json
        for shipment in shipments]


@app.route(
    '/<database_name>/flask_shipment_in_app/shipment/<int:id>',
    methods=['GET'])
@with_pool
@with_transaction()
@shipment_in_application
def get_shipment(request, pool, id):
    Shipment = pool.get('stock.shipment.in')

    shipment = Shipment(id)
    return shipment.flask_shipment_in_app_json


@app.route(
    '/<database_name>/flask_shipment_in_app/shipment/defaults',
    methods=['GET'])
@with_pool
@with_transaction()
@shipment_in_application
def shipment_defaults(request, pool):
    Shipment = pool.get('stock.shipment.in')
    return Shipment.default_get(Shipment._fields.keys())


@app.route(
    '/<database_name>/flask_shipment_in_app/shipment/create',
    methods=['POST'])
@with_pool
@with_transaction()
@shipment_in_application
def create_shipment(request, pool):
    Shipment = pool.get('stock.shipment.in')

    shipment = Shipment.create_from_flask_shipment_in_app(
        request.parsed_data.copy())
    shipment.save()

    return {
        'id': shipment.id,
        }


@app.route(
    '/<database_name>/flask_shipment_in_app/shipment/<int:id>/save',
    methods=['POST'])
@with_pool
@with_transaction()
@shipment_in_application
def save_shipment(request, pool, id):
    Shipment = pool.get('stock.shipment.in')
    Move = pool.get('stock.move')
    Product = pool.get('product.product')
    shipment = Shipment(id)
    data = request.parsed_data.copy()
    moves = data.get('moves', [])
    data.pop('moves') if 'moves' in data else None

    shipment.update_from_flask_shipment_in_app(data)
    shipment.save()

    if shipment.state == 'draft' and moves:
        app_quantities = defaultdict(float)
        for move in moves:
            app_quantities[move['product_id']] += move['quantity']

        to_save, to_delete = [], []
        for move in shipment.incoming_moves:
            app_quantity = app_quantities.pop(move.product.id, None)
            if app_quantity:
                move.quantity = app_quantity
                to_save.append(move)
            else:
                to_delete.append(move)
        Move.delete(to_delete)
        for product, quantity in app_quantities.items():
            move = Move()
            move.company = shipment.company
            move.shipment = shipment
            move.from_location = shipment.supplier_location
            move.to_location = shipment.warehouse_input
            move.product = Product(product)
            move.uom = move.product.default_uom
            move.quantity = quantity
            move.unit_price = move.product.cost_price
            to_save.append(move)
        Move.save(to_save)


@app.route(
    '/<database_name>/flask_shipment_in_app/shipment/<int:id>/receive',
    methods=['POST'])
@with_pool
@with_transaction()
@shipment_in_application
def receive_shipment(request, pool, id):
    Shipment = pool.get('stock.shipment.in')
    shipment = Shipment(id)

    Shipment.receive([shipment])
    Shipment.done([shipment])


@app.route(
    '/<database_name>/flask_shipment_in_app/shipment/<int:id>/delete',
    methods=['POST'])
@with_pool
@with_transaction()
@shipment_in_application
def delete_shipment(request, pool, id):
    Shipment = pool.get('stock.shipment.in')
    shipment = Shipment(id)

    Shipment.delete([shipment])


class CORSMiddleware(object):
    """Add Cross-origin resource sharing headers to every request."""

    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):

        def add_cors_headers(status, headers, exc_info=None):
            headers = Headers(headers)
            headers.add("Access-Control-Allow-Origin", '*')
            headers.add("Access-Control-Allow-Headers",
                'Origin, content-type, authorization, x-employee')
            headers.add("Access-Control-Allow-Credentials", "true")
            headers.add("Access-Control-Allow-Methods",
                "GET, POST, PUT, DELETE")
            return start_response(status, headers.to_list(), exc_info)

        if environ.get("REQUEST_METHOD") == "OPTIONS":
            add_cors_headers("200 Ok", [("Content-Type", "text/plain")])
            return [b'200 Ok']

        return self.app(environ, add_cors_headers)


app.wsgi_app = CORSMiddleware(app.wsgi_app)


app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
    '/flask_shipment_in_app/static':
        os.path.join(os.path.dirname(__file__), 'app')
})
