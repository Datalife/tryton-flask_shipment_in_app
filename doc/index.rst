Flask Shipment In App Module
############################

The module exposes a javascript application to receive supplier shipments.
In order to make it work you should build it's code inside the `app` folder.
This can be done with the following comand:

    $ npm install
    $ npm run build
    $ npm run compile-translations

Then you can access them directly from your tryton server using the following
route: http://server:port/flask_shipment_in_app

On the first run the application will as for the database and the username to
connect to. This will create an user application request on the user that
should be accepted to make it work. Once accepted the token will be used to
interact with tryton without requesting any password.
