# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import res
from . import routes
from . import stock

__all__ = ['register', 'routes']


def register():
    Pool.register(
        res.UserApplication,
        stock.ShipmentIn,
        module='flask_shipment_in_app', type_='model')
    Pool.register(
        module='flask_shipment_in_app', type_='wizard')
    Pool.register(
        module='flask_shipment_in_app', type_='report')
