# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import datetime

from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction


class ShipmentIn(metaclass=PoolMeta):
    __name__ = 'stock.shipment.in'

    @classmethod
    def create_from_flask_shipment_in_app(cls, data):
        pool = Pool()
        User = pool.get('res.user')

        user = User(Transaction().user)
        data['warehouse'] = user.stock_warehouse.id \
            if user.stock_warehouse else None
        if 'effective_date' in data:
            data['effective_date'] = datetime.datetime.strptime(
                data['effective_date'], '%Y-%m-%d').date()
        return cls(**data)

    def update_from_flask_shipment_in_app(self, data):
        if 'effective_date' in data:
            data['effective_date'] = datetime.datetime.strptime(
                data['effective_date'], '%Y-%m-%d').date()
        for field, value in data.items():
            setattr(self, field, value)

    @property
    def flask_shipment_in_app_json(self):
        attachments = [{
            'id': attachment.id,
            'name': attachment.name,
            'data': attachment.data}
            for attachment in self.attachments]

        return {
            'id': self.id,
            'number': self.number,
            'comment': self.comment,
            'supplier': self.supplier.rec_name,
            'planned_date': (self.planned_date.strftime('%Y-%m-%d')
                if self.planned_date else ''),
            'effective_date': (self.effective_date.strftime('%Y-%m-%d')
                if self.effective_date else ''),
            'state': self.state,
            'moves': [{
                    'product_id': m.product.id,
                    'product_name': m.product.rec_name,
                    'quantity': m.quantity,
                    } for m in self.incoming_moves],
            'external_picking': self.external_picking or '',
            'carrier_driver': self.carrier_driver or '',
            'carrier_email': self.carrier_email or '',
            'attachments': attachments
        }
