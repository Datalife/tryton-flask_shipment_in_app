var Vue = require('vue');
var VueGettext = require('vue-gettext')
var translations = require('./../translations.json')
var storage = require('./storage.js');
Vue.use(require('vue-router'));
Vue.use(require('vue-resource'));
Vue.use(VueGettext.default, {
    translations: translations,
    defaultLanguage: storage.language || 'es',
    availableLanguages: {
        en: 'English', 
        es: 'Castellano',
        fr: 'French',
    },
});

Vue.directive('focus', {
    inserted: function(el) {
        el.focus();
    }
});

Vue.directive('on-bs', {
    bind: function(el, binding, vnode) {
        var evt = binding.arg.replace(/-/g, '.');
        jQuery(el).on(evt, binding.value);
    }
});

var Main = require('./Main.vue');
var vm = new Main().$mount('#app');
