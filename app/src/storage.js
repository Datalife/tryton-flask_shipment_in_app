Storage.prototype.setObject = function(key, value) {
    this.setItem(key, JSON.stringify(value));
};
Storage.prototype.getObject = function(key) {
    var value = this.getItem(key);
    return value && JSON.parse(value);
};

var storage = {
    clear: function() {
        ['settings', 'employee'].forEach(function(name) {
            localStorage.removeItem('flask_shipment_in_app_' + name);
        });
        ['employees', 'languages'].forEach(function(name) {
            sessionStorage.removeItem('flask_shipment_in_app_' + name);
        });
    }
};
Object.defineProperty(storage, 'settings', {
    get: function() { return localStorage.getObject('flask_shipment_in_app_settings') || {}; },
    set: function(value) { localStorage.setObject('flask_shipment_in_app_settings', value); },
    enumerable: true,
    configurable: true
});
['language', 'employee'].forEach(function(name) {
    Object.defineProperty(storage, name, {
        get: function() { return localStorage.getItem('flask_shipment_in_app_' + name); },
        set: function(value) { localStorage.setItem('flask_shipment_in_app_' + name, value); },
        enumerable: true,
        configurable: true
    });
});
['employees', 'languages'].forEach(function(name) {
    Object.defineProperty(storage, name, {
        get: function() {
            return sessionStorage.getObject('flask_shipment_in_app_' + name);
        },
        set: function(value) {
            sessionStorage.setObject('flask_shipment_in_app_' + name, value);
        },
        enumerable: true,
        configurable: true
    });
});

module.exports = storage;
